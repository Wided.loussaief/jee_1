package managedBean;

import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import Services.eventService;
import model.Event;

@ManagedBean(name = "EventBean") 
@SessionScoped
public class EventBean {
	private static final long serialVersionUID = 1L;
	private int eventId;
	private String description;
	private Date dateBegin;
	private Date dateEnd;
	
	
	@EJB
	eventService eventservice;
	
	/////*******ADD EVENT************
	public String AddEvent() {
		Event event= new Event(dateBegin, dateEnd,description);
		Date d = new Date();
		event.setDateBegin(d);
		event.setDateEnd(d);
		eventservice.AddEvent(event);
		FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("add succes"));
		return "/Event/AddEvent";
	}
	
	
	
	
	
	public String getDescription() {
		return description;
	}





	public void setDescription(String description) {
		this.description = description;
	}





	public eventService getEventservice() {
		return eventservice;
	}





	public void setEventservice(eventService eventservice) {
		this.eventservice = eventservice;
	}





	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public Date getDateBegin() {
		return dateBegin;
	}
	public void setDateBegin(Date dateBegin) {
		this.dateBegin = dateBegin;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public EventBean() {
		super();
	}

}
