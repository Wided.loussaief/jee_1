package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Questionnaires database table.
 * 
 */
@Entity
@Table(name="Questionnaires")
@NamedQuery(name="Questionnaire.findAll", query="SELECT q FROM Questionnaire q")
public class Questionnaire implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="QuestionnaireId")
	private int questionnaireId;

	@Column(name="Question")
	private Object question;

	@Column(name="Type_Question")
	private Object type_Question;

	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="CompanyId")
	private Company company;

	//bi-directional many-to-one association to ReponseQuestion
	@OneToMany(mappedBy="questionnaire")
	private List<ReponseQuestion> reponseQuestions;

	public Questionnaire() {
	}

	public int getQuestionnaireId() {
		return this.questionnaireId;
	}

	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public Object getQuestion() {
		return this.question;
	}

	public void setQuestion(Object question) {
		this.question = question;
	}

	public Object getType_Question() {
		return this.type_Question;
	}

	public void setType_Question(Object type_Question) {
		this.type_Question = type_Question;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<ReponseQuestion> getReponseQuestions() {
		return this.reponseQuestions;
	}

	public void setReponseQuestions(List<ReponseQuestion> reponseQuestions) {
		this.reponseQuestions = reponseQuestions;
	}

	public ReponseQuestion addReponseQuestion(ReponseQuestion reponseQuestion) {
		getReponseQuestions().add(reponseQuestion);
		reponseQuestion.setQuestionnaire(this);

		return reponseQuestion;
	}

	public ReponseQuestion removeReponseQuestion(ReponseQuestion reponseQuestion) {
		getReponseQuestions().remove(reponseQuestion);
		reponseQuestion.setQuestionnaire(null);

		return reponseQuestion;
	}

}