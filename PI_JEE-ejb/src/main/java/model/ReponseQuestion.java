package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ReponseQuestions database table.
 * 
 */
@Entity
@Table(name="ReponseQuestions")
@NamedQuery(name="ReponseQuestion.findAll", query="SELECT r FROM ReponseQuestion r")
public class ReponseQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ReponseQuestionId")
	private int reponseQuestionId;

	//bi-directional many-to-one association to Questionnaire
	@ManyToOne
	@JoinColumn(name="QuestionId")
	private Questionnaire questionnaire;

	public ReponseQuestion() {
	}

	public int getReponseQuestionId() {
		return this.reponseQuestionId;
	}

	public void setReponseQuestionId(int reponseQuestionId) {
		this.reponseQuestionId = reponseQuestionId;
	}

	public Questionnaire getQuestionnaire() {
		return this.questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

}