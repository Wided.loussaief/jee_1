package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Certifications database table.
 * 
 */
@Entity
@Table(name="Certifications")
@NamedQuery(name="Certification.findAll", query="SELECT c FROM Certification c")
public class Certification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CertificationId")
	private int certificationId;

	@Column(name="Description")
	private Object description;

	public Certification() {
	}

	public int getCertificationId() {
		return this.certificationId;
	}

	public void setCertificationId(int certificationId) {
		this.certificationId = certificationId;
	}

	public Object getDescription() {
		return this.description;
	}

	public void setDescription(Object description) {
		this.description = description;
	}

}