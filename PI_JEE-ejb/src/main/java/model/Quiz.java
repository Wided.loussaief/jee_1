package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Quizs database table.
 * 
 */
@Entity
@Table(name="Quizs")
@NamedQuery(name="Quiz.findAll", query="SELECT q FROM Quiz q")
public class Quiz implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="QuizId")
	private int quizId;

	//bi-directional many-to-one association to Candidature
	@ManyToOne
	@JoinColumn(name="CandidatureId")
	private Candidature candidature;

	//bi-directional many-to-one association to ResultQuiz
	@OneToMany(mappedBy="quiz")
	private List<ResultQuiz> resultQuizs;

	public Quiz() {
	}

	public int getQuizId() {
		return this.quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public Candidature getCandidature() {
		return this.candidature;
	}

	public void setCandidature(Candidature candidature) {
		this.candidature = candidature;
	}

	public List<ResultQuiz> getResultQuizs() {
		return this.resultQuizs;
	}

	public void setResultQuizs(List<ResultQuiz> resultQuizs) {
		this.resultQuizs = resultQuizs;
	}

	public ResultQuiz addResultQuiz(ResultQuiz resultQuiz) {
		getResultQuizs().add(resultQuiz);
		resultQuiz.setQuiz(this);

		return resultQuiz;
	}

	public ResultQuiz removeResultQuiz(ResultQuiz resultQuiz) {
		getResultQuizs().remove(resultQuiz);
		resultQuiz.setQuiz(null);

		return resultQuiz;
	}

}