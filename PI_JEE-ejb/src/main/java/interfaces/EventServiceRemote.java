package interfaces;

import javax.ejb.Remote;

import model.Event;

@Remote
public interface EventServiceRemote {

	public void AddEvent(Event e);
}
