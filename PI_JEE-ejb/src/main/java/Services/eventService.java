package Services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.EventServiceRemote;
import model.Event;

@Stateless
@LocalBean
public class eventService implements EventServiceRemote {

	@PersistenceContext(unitName="imputation-ejb")
	EntityManager em;
	
	@Override
	public void AddEvent(Event e){ em.persist(e); }

}
